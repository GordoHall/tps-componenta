import pymongo
import time
import os

mongo_host = os.getenv('MONGO_HOST', 'localhost')
mongo_port = os.getenv('MONGO_PORT', 27017)

client = pymongo.MongoClient(host=mongo_host, port=int(mongo_port))


db = client["tradedb"]  # select the database
trade = db.trade  # select the collection
trade.create_index([("$**", "text")])


def Main():
    while True:
        update_records_state("CREATED", "PENDING")

        time.sleep(5)
        update_records_state("PENDING", "FILLED")


def update_records_state(state_filter, end_state):
    stocks = all_records(state_filter)
    # update records
    for each in stocks:
        print('updating')
        id = each['_id']
        trade.update_one({'_id': id}, {'$set': {'state': end_state}})


def all_records(state):
    cursor = trade.find({'state': state})
    stocks = []
    for record in cursor:
        stocks.append(record)

    return stocks


if __name__ == "__main__":
    Main()
